﻿using Newtonsoft.Json;
using RaceLogic;
using RaceLogic.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace LazyBearsRace
{
    class Program
    {
        static void Main(string[] args)
        {
            Settings settings = Program.GetSettings();

            List<IElement> bears = new List<IElement>(){
                new Element("Perezoso"),
                new Element("Panda"),
                new Element("Peluche")
            };

            Race race = new Race(bears, settings.RacetrackLength);
            race.StartRace();

            Console.WriteLine("Hello World!");
            Console.WriteLine("Racetrack length is " + settings.RacetrackLength);

            Console.ReadKey();
        }

        static Settings GetSettings()
        {
            string settingsPath = AppDomain.CurrentDomain.BaseDirectory + "settings.json";

            Settings settings;
            using (StreamReader file = File.OpenText(settingsPath))
            {
                JsonSerializer serializer = new JsonSerializer();
                settings = (Settings)serializer.Deserialize(file, typeof(Settings));
            }

            return settings;
        }
    }

    public class Settings
    {
        public int RacetrackLength { get; set; }
    }
}
