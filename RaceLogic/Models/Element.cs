﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RaceLogic.Models
{
    public class Element : IElement, IMovable
    {
        public string Name { get; set; }
        public int DistanceTraveled { get; set; }
        public bool IsOutOfCompetence { get; set; }

        public Element(string name)
        {
            this.Name = name;
            this.DistanceTraveled = 0;
            this.IsOutOfCompetence = false;
        }

        public RaceResult RunTillTheEnd(int racetrackLength)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            Console.WriteLine(Name + " is starting to run!");

            while (DistanceTraveled < racetrackLength)
            {
                Random random = new Random();
                var goAhead = random.Next(1, 30);

                MoveForward(goAhead);

                if (goAhead > 0 && goAhead <= 10)
                    Rest(800);
                else if (goAhead > 10 && goAhead <= 20)
                    Rest(1700);
                else
                    Rest(2600);

                DistanceTraveled += goAhead;
            }

            watch.Stop();

            return new RaceResult()
            {
                Status = RaceStatus.FINISHED,
                TimeElapsed = TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds)
            };
        }

        public void MoveForward(int distance)
        {
            Console.WriteLine("Moving forward " + distance + " steps");

            this.DistanceTraveled += distance;
        }

        public void Rest(int miliseconds)
        {
            Console.WriteLine("He fell asleep!");
            System.Threading.Thread.Sleep(miliseconds);
            Console.WriteLine("Get up after " + miliseconds + " milliseconds");
        }

        public void Stop()
        {
            if (!IsOutOfCompetence)
                IsOutOfCompetence = true;
        }
    }
}
