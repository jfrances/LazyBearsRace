﻿using System;

namespace RaceLogic.Models
{
    public interface IMovable
    {
        int DistanceTraveled { get; set; }
        void MoveForward(int distance);
        void Rest(int miliseconds);
        void Stop();
    }
}
