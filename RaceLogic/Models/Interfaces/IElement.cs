﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RaceLogic.Models
{
    public interface IElement
    {
        string Name { get; set; }
        bool IsOutOfCompetence { get; set; }
        RaceResult RunTillTheEnd(int racetrackLength);
        void MoveForward(int distance);
        void Rest(int milliseconds);
        void Stop();
        
    }
}
