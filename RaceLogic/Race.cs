﻿using RaceLogic.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RaceLogic
{
    public class Race
    {
        public List<IElement> Competitors { get; set; }
        public int RacetrackLength { get; set; }
        public RaceResult Result { get; set; }

        public Race(List<IElement> competitors, int racetrackLength)
        {
            this.Competitors = competitors;
            this.RacetrackLength = racetrackLength;
        }

        public void StartRace()
        {
            // Create threads
            foreach (var comp in Competitors)
            {
                var result = comp.RunTillTheEnd(RacetrackLength);
                Console.WriteLine("- - - - - - - - - - - - - - - -");
                Console.WriteLine(comp.Name);
                Console.WriteLine(result.ToString());
                Console.WriteLine("- - - - - - - - - - - - - - - -");
                System.Threading.Thread.Sleep(5000);
                Console.Clear();
            }
        }
    }
}
