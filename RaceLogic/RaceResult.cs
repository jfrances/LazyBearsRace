﻿using RaceLogic.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RaceLogic
{
    public class RaceResult
    {
        public RaceStatus Status { get; set; }
        public TimeSpan TimeElapsed { get; set; }

        public override string ToString()
        {
            string result = string.Empty;

            result += "\nStatus: " + Status.ToString();
            result += "\nTime elapsed: " + TimeElapsed.ToString();

            return result;
        }
    }

    public enum RaceStatus
    {
        PLANNED,
        READY_TO_GO,
        IN_PROGRESS,
        FINISHED,
        ABANDONED
    }
}
