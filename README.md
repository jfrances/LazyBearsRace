# Lazy Bears Race
## Bears running for some honey!

#### Goal
Understand and learn about threads

#### What to follow
* User decide when to start race
* Three elements running for it
* Every step is random number between 1 and 30
* Sleep after each step
	* 	Sleep 800 milisecs if last step distance is between 1 - 10 
	* 	Sleep 1700 milisecs if last step distance is between 11 - 20
	* 	Sleep 2600 milisecs if last step distance is between 21 - 30 
* Total distance 1000



